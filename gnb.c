#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>
#include <stdlib.h>
#include <stdbool.h>

#include "gbn.h"

#define MAX_MESSAGES 20
#define MESSAGE_LENGTH 200
#define TTL (int)(0.1 * CLOCKS_PER_SEC)
#define TTW (int)(0.2 * CLOCKS_PER_SEC)
#define MSG_DELAY 0

#define END_SIG MAX_MESSAGES + 3

#define SIGNAL_PAUSE 0
#define SIGNAL_RESEND 1
#define SIGNAL_NEXT 2
#define SIGNAL_END 3

char messages[MAX_MESSAGES][MESSAGE_LENGTH];
int position;
int iter = 0;

void gbn_fserver(cfg_t * cfg) {
	int server, size = sizeof(struct sockaddr_in), hold, client, last = MAX_MESSAGES - 1;
	socklen_t * length = sizeof(struct sockaddr_in);	
	char buf [MESSAGE_LENGTH + 6], responce [10];
	char address [39];
	clock_t t;
	if ((server = socket(AF_INET, SOCK_DGRAM, 0)) >= 0){
		struct sockaddr_in addressinfo; memset(&addressinfo, 0, sizeof(struct sockaddr_in));
		addressinfo.sin_family = AF_INET;
		addressinfo.sin_addr.s_addr = INADDR_ANY;
		addressinfo.sin_port = htons(cfg->port);
		if ((hold = bind(server, (struct sockaddr *) &addressinfo, sizeof(addressinfo))) == 0) {
			int sentry = 1;
			while (sentry){
				struct sockaddr_in clientaddressinfo;
				memset(buf, '\0', sizeof(char) * (MESSAGE_LENGTH + 6));
				memset(messages, '\0', sizeof(char) * MAX_MESSAGES * MESSAGE_LENGTH);
				memset(responce, 0, sizeof(char) * 10);
				memset(&clientaddressinfo, 0, sizeof(struct sockaddr_in));
				position = -1;
				int priorposition = 0;
				while (1){
					t = clock();
					bool trip = true;
					bool skip = false;
					while(clock() - t < TTL){
						if(1 < recvfrom(server, buf, MESSAGE_LENGTH + 6,
								MSG_DONTWAIT, (struct sockaddr *) &clientaddressinfo, &size)){

							int sto = ((buf[0] - '0') * 100) + ((buf[1] - '0') * 10) + (buf[2] - '0');
							int citer = buf[3] - '0';
							printf("Position: %d! Number: %d! Iter: %d! Citer: %d!\n", position, sto, iter, citer);
							
							if (citer != iter){
								skip = true;
								printf("c!i\n");
							} else {
								skip = false;
							}
							
							if (sto == END_SIG) {
								trip = false;
								sentry = 0;
								printf("e\n");
								last = priorposition + 1;
								memset(messages[last], '\0', sizeof(char) * MESSAGE_LENGTH);
								break;
							} else if (sto > -1 && sto < MAX_MESSAGES){
								for(int i = 0; i < MESSAGE_LENGTH; i++) {
									messages[sto][i] = buf[i + 4];
								}
								printf("w\n");
							}
							priorposition = sto;
							if (position + 1 == sto) { position++; }
						}
						if (position == MAX_MESSAGES - 1) { break; }
					}
					//sprintf(responce, "%d %d", SIGNAL_PAUSE, 0);

						
					if(skip) {
						printf("already got block, asking for next (iter:%d)\n", iter);
						sprintf(responce, "%d %d", SIGNAL_NEXT, 0);
						sendto(server, responce, 10, 0, (const struct sockaddr *) &clientaddressinfo, size);
						iter = (iter + 1) % 10;
					} else if (position < last - 1){
						printf("asking for resend!\n");
						sprintf(responce, "%d %d", SIGNAL_RESEND, position + 1);
						sendto(server, responce, 10, 0, (const struct sockaddr *) &clientaddressinfo, size);
					} else {
						break;
					}
				}
				printf("printing text!");	
				for(int i = 0; i <= last; i++){
					//fwrite("\nSTART BLOCK\n", sizeof(char), 13, cfg->fstream);
					for (int j = 0; j < MESSAGE_LENGTH && messages[i][j] != '\0'; j++){
					//for (int j = 0; j < MESSAGE_LENGTH; j++) {
					//	if (messages[i][j] != '\0'){
						fputc(messages[i][j], cfg->fstream);
					}
					//fwrite(messages[i], sizeof(char), MESSAGE_LENGTH, cfg->fstream);
				}
				if (last == MAX_MESSAGES - 1){
					sprintf(responce, "%d %d", SIGNAL_NEXT, 0);
					sendto(server, responce, 10, 0, (const struct sockaddr *) &clientaddressinfo, size);
					iter = (iter + 1) % 10;
				} else {
					sprintf(responce, "%d %d", SIGNAL_END, 0);
					sendto(server, responce, 10, 0, (const struct sockaddr *) &clientaddressinfo, size);
				}
				fflush(cfg->fstream);
			}
		} else {
			printf("Sock bind failed with error code %d\n", hold);
		}
	} else {
		printf("Socket handle not valid!  socket() returned a 0\n");
	}

	/* !!!! DEBUGGING !!!! */
	for (int i = 0; i < MAX_MESSAGES; i++){
		printf("      !!!!!! BLOCK %d: !!!!!!\n", i);
		for (int j = 0; j < MESSAGE_LENGTH; j++){
			printf("%c", messages[i][j]);
		}
		printf("\n");
	}
	/* !!!! DEBUGGING !!!! */
	close (server);
}

void gbn_fclient(cfg_t * cfg) {
	int server, opt = 1, size = sizeof(struct sockaddr_in), hold, client;
	char buf [MESSAGE_LENGTH + 6], responce[10];
	int sentry = 1, end = -1, bt = 'a';
	clock_t t;
	if ((server = socket(AF_INET, SOCK_DGRAM, 0)) >= 0){
		struct sockaddr_in addressinfo; memset(&addressinfo, 0, sizeof(struct sockaddr_in));
		addressinfo.sin_family = AF_INET;
		addressinfo.sin_addr.s_addr = INADDR_ANY;
		addressinfo.sin_port = htons(cfg->port);
		while (sentry){
			struct sockaddr_in clientaddressinfo;
			memset(buf, 0, sizeof(char) * (MESSAGE_LENGTH + 6));
			memset(messages, '\0', sizeof(char) * MAX_MESSAGES * MESSAGE_LENGTH);
			memset(responce, 0, sizeof(char) * 10);
			memset(&clientaddressinfo, 0, sizeof(struct sockaddr_in));
			//if (cfg->fstream == NULL) { printf("bad dog\n"); return; }
			for (int j = 0; j < MAX_MESSAGES && bt != EOF; j++){
				printf("Reading block %d %d\n", iter, j);
				//if (ferror(cfg->fstream) != 0 ){printf("fstream error: %d\n", ferror(cfg->fstream));}
				if(MESSAGE_LENGTH != fread(messages[j], sizeof(char), MESSAGE_LENGTH, cfg->fstream)){
					end = j + 1;
					break;
				}
				
				/*for (int i = 0; i < MESSAGE_LENGTH && (bt = fgetc(cfg->fstream)) != EOF; i++){
					printf("%c", (char)bt);
					messages[j][i] = (char)bt;
				}
				printf("\n");	
				if (bt == EOF){
					end = j + 1;
					printf("reached eof\n");
					break;
				}*/
			}

			bool repeat = true;

			int start = 0;
			while (repeat){
				bool responded = true;
				for (int i = start; i < MAX_MESSAGES; i++){
					
					if (end == i){
						printf("sending end signal~!\n");
						sprintf(buf, "%03d0000", END_SIG);
						i = END_SIG;
						sentry = false;
						sendto(server, buf, MESSAGE_LENGTH + 6, 0, (const struct sockaddr *) &addressinfo, size);
					} else {
						snprintf(buf, MESSAGE_LENGTH + 6, "%03d%d%s", i, iter, messages[i]);
						sendto(server, buf, MESSAGE_LENGTH + 6, 0, (const struct sockaddr *) &addressinfo, size);
					}
				}
				t = clock();
				while(1 > recvfrom(server, responce, 10, MSG_DONTWAIT, (struct sockaddr *) &addressinfo, &size)){
					if (clock() - t > TTL){
						responded = false;
						printf("rebroadcasting!\n");
						break;
					}
				}
				if (responded) {
					int sig;
					sscanf(responce, "%d %d", &sig, &start);
					if (sig == SIGNAL_NEXT && sentry){ repeat = false; }
					if (sig == SIGNAL_END){ repeat = false; }
				}
			}
			printf("Current iter: %d\n", iter);
			iter = (iter + 1) % 10;
		}
	} else {
		printf("Socket handle not valid!  socket() returned a 0\n");
	}
	close (server);
}
